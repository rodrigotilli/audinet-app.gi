import Vue from "vue";
import VueAWN from "vue-awesome-notifications";

// Your custom options
let options = {
  icons: {
    enabled: true,
    prefix: "<i class='mdi mdi-",
    suffix: "'></i>",
    success: "check-circle"
  },
  labels: {
    info: "Info",
    success: "Éxito!",
    warning: "Atención",
    alert: "Error"
  }
};

Vue.use(VueAWN, options);
