import qs from "qs";
export const state = () => ({
  usuarios: {},
  errorConection: false,
  searchUrl: ""
});

export const mutations = {
  SET_USUARIO(state, data) {
    state.usuarios = data;
  },
  SET_ERROR_CONECTION(state, data) {
    state.errorConection = data;
  },
  SET_SEARCH_URL(state, data) {
    state.searchUrl = data;
  }
};

export const actions = {
  async getList({ commit, state }, params) {
    try {
      let sort;
      // verificar que los datos existan para que sortDesc[0] no arroje -> property 0 of undefined / devuelve Boolean
      if (params && params.sortDesc) sort = params.sortDesc[0];
      console.log("sort es: " + sort);
      // determina si la direccion es asc o desc depediendo el resultado de sort
      let direction = sort ? "desc" : "asc";
      console.log("direction es: " + direction);
      if (state.searchUrl) {
        commit(
          "SET_USUARIO",
          await this.$axios.$get(state.searchUrl, {
            params: {
              page: params.page,
              per_page: params.itemsPerPage,
              direction: direction,
              // direction: direction,
              // ...(direction ? { direction: "desc" } : { direction: "asc" }),
              column: params.sortBy
            },
            paramsSerializer: params => {
              return qs.stringify(params, { arrayFormat: "repeat" });
            }
          })
        );
      } else {
        commit(
          "SET_USUARIO",
          await this.$axios.$get("/usuario-paginado", {
            params: {
              page: params.page,
              per_page: params.itemsPerPage,
              direction: direction,
              // direction: direction,
              // ...(direction ? { direction: "desc" } : { direction: "asc" }),
              column: params.sortBy
            },
            paramsSerializer: params => {
              return qs.stringify(params, { arrayFormat: "repeat" });
            }
          })
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
};
