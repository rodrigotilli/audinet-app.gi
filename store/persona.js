import qs from "qs";
export const state = () => ({
  personas: {},
  personaDataForm: {},
  errorConection: false,
  searchUrl: ""
});

export const mutations = {
  SET_PERSON(state, data) {
    state.personas = data;
  },
  SET_PERSONA_DATA_FORM(state, data) {
    state.personaDataForm = data;
  },
  SET_ERROR_CONECTION(state, data) {
    state.errorConection = data;
  },
  SET_SEARCH_URL(state, data) {
    state.searchUrl = data;
  }
};

export const actions = {
  // OBTIENE LISTA DE DOCUMENTOS, GENEROS Y ESTADO PARA FORMULARIOS
  async getPersonaDataForm({ commit }) {
    try {
      commit(
        "SET_PERSONA_DATA_FORM",
        await this.$axios.$get("/persona-data-form")
      );
    } catch (error) {
      console.error("el error es: " + error);
    }
  },
  async getList({ commit, state }, params) {
    try {
      let sort;
      // verificar que los datos existan para que sortDesc[0] no arroje -> property 0 of undefined / devuelve Boolean
      if (params && params.sortDesc) sort = params.sortDesc[0];
      // determina si la direccion es asc o desc depediendo el resultado de sort
      let direction = sort ? "desc" : "asc";
      // VERIFICA SI EXISTE UN PARAMETRO DE BUSQUEDA EN LA VARIABLE SEARCHURL Y OBTIENE LA LISTA DE PERSONAS
      if (state.searchUrl) {
        commit(
          "SET_PERSON",
          await this.$axios.$get(state.searchUrl, {
            params: {
              page: params.page,
              per_page: params.itemsPerPage,
              direction: direction,
              column: params.sortBy
            },
            paramsSerializer: params => {
              return qs.stringify(params, { arrayFormat: "repeat" });
            }
          })
        );
      } else {
        commit(
          "SET_PERSON",
          await this.$axios.$get("/persona-paginado", {
            params: {
              page: params.page,
              per_page: params.itemsPerPage,
              direction: direction,
              column: params.sortBy
            },
            paramsSerializer: params => {
              return qs.stringify(params, { arrayFormat: "repeat" });
            }
          })
        );
      }
    } catch (error) {
      commit("SET_ERROR_CONECTION", true);
      console.error("el error es: " + error);
    }
  },
  // RECARGAR PERSONA
  async reload({ commit }) {
    try {
      commit("SET_SEARCH_URL", "");
      commit("SET_PERSON", await this.$axios.$get("persona-paginado"));
    } catch (error) {
      console.error("el error es: " + error);
    }
  },
  // CREAR PERSONA NUEVA
  async nuevaPersona({ context }, form) {
    try {
      await this.$axios.$post("/persona", {
        nombre: form.nombre,
        onombre: form.onombre,
        apellido: form.apellido,
        oapellido: form.oapellido,
        email: form.email,
        celular: form.celular,
        f_nacimiento: form.f_nacimiento,
        genero_id: form.genero_id,
        tipo_documento_id: form.tipo_documento_id,
        nro_documento: form.nro_documento,
        cuit: form.cuit,
        estado_id: form.estado_id,
        foto: form.foto
      });
    } catch (error) {
      console.error("el error es: " + error);
    }
  },
  // ACTUALIZAR PERSONA
  async actualizarPersona({ context }, form) {
    await this.$axios.$patch("/persona", form);
  }
};
